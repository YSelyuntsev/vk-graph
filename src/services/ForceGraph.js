import ForceGraph from "force-graph";

export default {
  _graph: null,

  _options: {
    /* Interaction */
    zoom: 0,
    maxZoom: 8,
    interactionTime: 800,
    cooldownTime: 7000,

    /* Nodes style */
    enableNodeDrag: false,
    nodeLabel: "",

    /* Link style */
    linkWidth: 0.5,
    linkWidthHover: 2,
    linkColor: "rgba(255, 255, 255, .2)",
    linkColorHover: "rgba(255, 255, 255, .96)",

    /* Data */
    container: "",
    highlight: null,
    data: {
      nodes: [],
      links: []
    }
  },

  generate(options) {
    this._options = Object.assign(this._options, options);

    if (this._options.container) {
      this._graph = ForceGraph();

      this._graph(this._options.container)
        .enableNodeDrag(this._options.enableNodeDrag)
        .cooldownTime(this._options.cooldownTime)
        .nodeLabel(this._options.nodeLabel)
        .nodeAutoColorBy("")

        .d3AlphaDecay(0.1)
        .linkCurvature(0.24)

        .onNodeClick(node => {
          node
            ? (this._options.highlight = node.id)
            : (this._options.highlight = null);

          document.dispatchEvent(
            new CustomEvent("onNodeClick", { detail: node })
          );
        })

        .linkWidth(link => {
          return link.target.id === this._options.highlight ||
            link.source.id === this._options.highlight
            ? this._options.linkWidthHover
            : this._options.linkWidth;
        })

        .linkColor(link => {
          return link.target.id === this._options.highlight ||
            link.source.id === this._options.highlight
            ? this._options.linkColorHover
            : this._options.linkColor;
        })

        .graphData(this._options.data);

      if (!this._options.is3d) {
        this._graph.nodeCanvasObject((node, ctx) => {
          if (node.id === this._options.highlight) {
            ctx.beginPath();
            ctx.arc(node.x, node.y, 6, 0, 2 * Math.PI, false);
            ctx.fillStyle = "#ffffff";
            ctx.fill();
          }
          ctx.beginPath();
          ctx.arc(node.x, node.y, 4, 0, 2 * Math.PI, false);
          ctx.fillStyle = node.color;
          ctx.fill();
        });
        this._options.zoom = this._graph.zoom();
      }
    } else {
      console.error("No find container");
    }
  },

  moveTo(id) {
    let node = this._graph.graphData().nodes.find(node => node.id === id);

    if (this._graph.zoom() == this._options.maxZoom) {
      this._graph.zoom(
        this._options.zoom * 2,
        this._options.interactionTime / 1.5
      );
      setTimeout(() => {
        this._camera(node);
      }, this._options.interactionTime / 1.5);
    } else {
      this._camera(node);
    }
  },

  _camera(node) {
    this._graph.centerAt(node.x, node.y, this._options.interactionTime / 1.5);
    this._graph.zoom(this._options.maxZoom, this._options.interactionTime);
  },

  highlight(id = null) {
    this._options.highlight = id;
  }
};
