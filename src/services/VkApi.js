import Vue from "vue";
import jsonp from "vue-jsonp";

Vue.use(jsonp, 10000);

const token =
  "a7d07923a7d07923a7d07923eaa7b70a63aa7d0a7d07923fbdfdcb91b456d92f00b72e5";

const request = (id, user, request, params) => {
  return Vue.jsonp(
    `https://api.vk.com/method/${request}?${user}=${id}&fields=${params}&access_token=${token}&v=5.92`
  );
};

export default {
  async getUser(id, callback) {
    try {
      const data = await request(
        id,
        "user_ids",
        "users.get",
        "photo_50,city,domain,photo_max"
      );

      return data.response ? data.response[0] : null;
    } catch (error) {
      callback(error);
    }
  },

  async getUserFriends(id, callback) {
    try {
      const data = await request(
        id,
        "user_id",
        "friends.get",
        "domain,city,photo_50,photo_max,deactivated,education,interests,about,relation"
      );

      return data.response ? data.response.items : null;
    } catch (error) {
      callback(error);
    }
  }
};
