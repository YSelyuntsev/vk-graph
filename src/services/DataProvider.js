const _createNode = (node = {}, parent = 0) => {
  return {
    ...node,
    loaded: false,
    parent,
    label: `${node.first_name} ${node.last_name}`,
    title: `
      <img class="graph-tooltip-image" src="${node.photo_50}">
      <p>${node.first_name} ${node.last_name}</p>
    `
  };
};

const _createLink = (source = 0, target = 0) => {
  return { source, target };
};

export default {
  createNodes(data = [], parent = 0) {
    return data.map(node => _createNode(node, parent));
  },

  createLinks(nodes = [], target = 0) {
    return nodes.map(node => ({ source: node.id, target }));
  },

  addNode(node = {}, parent = 0) {
    return _createNode(node, parent);
  },

  addLink(source = 0, target = 0) {
    return _createLink(source, target);
  },

  setLoaded(nodes = [], id = null) {
    nodes.find(node => (node.id === id ? (node.loaded = true) : 0));
  }
};
