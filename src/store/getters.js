/**
 * Getters of App Store Vuex
 */

export default {
  searchResult(state) {
    if (state.search.length > 2) {
      return state.data.nodes.filter(node => {
        if (
          node.label.toLowerCase().match(state.search.toLowerCase()) !== null &&
          state.search != ""
        )
          return node;
      });
    } else {
      return [];
    }
  }
};
