/**
 * Actions of App Store Vuex
 */

import ForceGraph from "../services/ForceGraph";
import DataProvider from "../services/DataProvider";
import VkApi from "../services/VkApi";

const createError = (type, message) => {
  return { type, message, created: new Date().toLocaleString() };
};

export default {
  generateGraph({ state, commit }) {
    ForceGraph.generate({
      container: document.getElementById("network"),
      nodeLabel: "title",
      data: state.data
    });

    commit("TOGGLE_LOADED");
  },

  // Disable linter for empty object argument
  // eslint-disable-next-line
  moveTo({}, id) {
    ForceGraph.moveTo(id);
  },

  selectUser({ commit }, user = {}) {
    commit("UPDATE_CURRENT_USER", user);
  },

  async getUser({ state, commit, dispatch }, callback) {
    let id = state.link.split("vk.com/").pop(-1);

    if (!id)
      return callback(
        createError("error", "Сслыка на профиль не может быть пустой!")
      );

    const user = await VkApi.getUser(id, error => console.log(error));
    const friends = await VkApi.getUserFriends(user.id, error =>
      console.log(error)
    );

    user.countFriends = friends.length;

    commit("UPDATE_USER", user);
    commit("CREATE_DATA", { user, friends });

    dispatch("generateGraph");
  },

  async loadFriends({ state, commit }, id) {
    const friends = await VkApi.getUserFriends(id, error => console.log(error));

    let data = state.data;

    commit("UPDATE_FRIENDS", { id, countFriends: friends.length });

    for (const user of friends) {
      let nodeIndex = data.nodes.findIndex(node => node.id === user.id);
      let linkIndex = data.links.findIndex(
        link => link.source.id + link.target.id === user.id + id
      );

      if (linkIndex === -1) data.links.push(DataProvider.addLink(user.id, id));

      if (nodeIndex === -1 && user.id !== id)
        data.nodes.push(DataProvider.addNode(user, id));
    }

    commit("LOAD_FRIENDS");
    commit("UPDATE_DATA", data);

    ForceGraph._graph.graphData(state.data);
  }
};
