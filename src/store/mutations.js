/**
 * Mutations of App Store Vuex
 */

import ForceGraph from "../services/ForceGraph";
import DataProvider from "../services/DataProvider";

export default {
  TOGGLE_LOADED(state) {
    state.loaded = !state.loaded;
  },

  UPDATE_SEARCH(state, value) {
    state.search = value;
  },

  UPDATE_LINK(state, value) {
    state.link = value;
  },

  UPDATE_USER(state, user) {
    state.user = user;
  },

  UPDATE_FRIENDS(state, payload) {
    state.data.nodes.find(node => {
      if (node.id === payload.id) node.countFriends = payload.countFriends;
    });
  },

  LOAD_FRIENDS(state) {
    state.currentUser.loaded = true;
  },

  CREATE_DATA(state, data) {
    state.data.nodes = DataProvider.createNodes(data.friends, data.user.id);
    state.data.links = DataProvider.createLinks(state.data.nodes, data.user.id);

    state.data.nodes.push(DataProvider.addNode(data.user, data.user.id));

    DataProvider.setLoaded(state.data.nodes, data.user.id);
  },

  UPDATE_CURRENT_USER(state, user = {}) {
    user.id ? ForceGraph.highlight(user.id) : ForceGraph.highlight();
    state.currentUser = user;
  },

  UPDATE_DATA(state, data) {
    state.data = data;
  }
};
