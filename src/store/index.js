import Vue from "vue";
import Vuex from "vuex";

import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    link: "",
    user: {},
    currentUser: {},
    data: { nodes: [], links: [] },
    search: "",
    loaded: false
  },

  getters,
  mutations,
  actions
});
